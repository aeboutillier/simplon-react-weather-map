import "../Styles/InfoMap.css";


function InfoMap(props) {
  let fabrique = props.tab;
  return (
    <div className="bgDetails">
      <div id="mapDetails">
        <h2>{fabrique[props.numIndex].nom}</h2>
        <p>{fabrique[props.numIndex].infos}</p>
        <p>{fabrique[props.numIndex].formation}</p>
      </div>
    </div>
  );
}

export default InfoMap;
