import "../Styles/AppMap.css";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useState } from "react";
import InfoMap from "./InfoMap";
import fabrique from "../simplonFab.json";

import L from "leaflet";

var redIcone = new L.Icon({
  iconUrl:
    "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",
  shadowUrl:
    "https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png",
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41],
});

const position = [43.183766, 3.004212];

function AppMap() {
  
  const [i, setI] = useState(0);
  const [soleil, setSoleil] = useState();
  const [geoloc, setGeoloc] = useState();
  const [date, setDate] = useState();
  
  let iconImg ='http://openweathermap.org/img/w/' + soleil?.weather[0].icon + '.png';

  navigator.geolocation.getCurrentPosition((location) => { 
  setGeoloc([location.coords.latitude, location.coords.longitude])
  })

  
  function afficherDate() {
    
      var cejour = new Date();
      var options = {weekday: "long", year: "numeric", month: "long", day: "2-digit"};
      var date = cejour.toLocaleDateString("fr-FR", options);
      var heure = ("0" + cejour.getHours()).slice(-2) + ":" + ("0" + cejour.getMinutes()).slice(-2);
      var dateheure = date + " " + heure;
      var dateheure = dateheure.replace(/(^\w{1})|(\s+\w{1})/g, lettre => lettre.toUpperCase());
      setDate(dateheure);
    
  }

  function getIconWeather(name) {
    if (name === undefined) {
      return "";
    } else {
      return "http://openweathermap.org/img/wn/" + name + "@2x.png";
    }
  }

  function changerIndex(index) {
    setI(index);
  }

  function meteo(lat,long) {

    fetch('https://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon=' + long + '&appid=45b95c3daba4851a9f5a9eca951a6eb9&units=metric&lang=fr')
    .then(response => response.json())
    .then(promise => {
      let jsonTab = promise;
      setSoleil(jsonTab);
    })
    .catch((err) => {console.log(err)});
 
  }

  return (
    <div id="map">
      <InfoMap numIndex={i} tab={fabrique}></InfoMap>
      <MapContainer center={position} zoom={7}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {fabrique.map((elem, index) => (
          
          <Marker
            position={[elem.latitude, elem.longitude]} key={index} icon={redIcone}
            eventHandlers={{
              click: () => {
                changerIndex(index);
                afficherDate();
                meteo(elem.latitude, elem.longitude);
              },
            }}>
        
          <Popup>
            <h2>{soleil?.name}</h2>
            <span id="dateheure">{date}</span> 
            <div className="meteoDesc">
            <img src={getIconWeather(soleil?.weather[0].icon)}alt="icon-weather"/>
              <p>{soleil?.weather[0].description}</p>
              <p>{soleil?.main.temp}C°</p>
            </div>

            <div className="meteoInfo">
              <p>Vents {Math.round(soleil?.wind.speed * 3.6)}Km/h</p>
              <p>Taux d'humidité {soleil?.main.humidity}%</p>
              <p>Pression {soleil?.main.pressure}Pa</p>
            </div>
          
          </Popup>
          
          </Marker>
        ))}

        
        <Marker position={geoloc} key="truc"
        ></Marker>

  

      </MapContainer>
    </div>
  );
}

export default AppMap;
