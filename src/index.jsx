import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import AppMap from "./Components/AppMap";

ReactDOM.render(
  <React.StrictMode>
    <AppMap />
  </React.StrictMode>,
  document.getElementById("mapBox")
);

// import got from "got";

// const scrapeData = async () => {
//   got("https://fr.wikipedia.org/wiki/Web_scraping") // Mettez le lien du site
//     .then(async (response) => {
//       console.log(response.body);
//     })
//     .catch((err) => {
//       console.log(err);
//     });
// };

// scrapeData();
